program xdlabrd
	implicit none
	integer :: m, n, nb, lda, ldx, ldy, info
	double precision, allocatable :: A(:,:),AA(:,:), D(:), E(:), X(:,:), Y(:,:)
	double precision, allocatable::  TAUQ(:), TAUP(:), WORK(:)
	integer :: argc, minmn, i, j
	character(len=31) :: arg
	double precision, parameter :: one = 1.0D+0
	double precision :: ad
	
	interface
		function mean(A)
			double precision :: A(:), mean
		end function
		function variance(A)
			double precision :: A(:), variance
		end function
	end interface

	argc = command_argument_count()
	if( argc .ne. 2 ) then
		print *,'argc=', argc
		print *, 'usage: xdgebrd m n'
		call exit(1)
	end if
	
	call get_command_argument(1, arg)
	read (arg, '(I10)') m
	call get_command_argument(2, arg)
	read (arg, '(I10)') n

	print *,'DLABRD:m,n=',m,n
	minmn = min(m, n)
	lda = m+1
	ldx = m+1
	ldy = n+1
	!nb = 2

	allocate( A(m+1,n+1), AA(m+1,n+1) )
	allocate( D(minmn+1), E(minmn+1), TAUQ(minmn+1), TAUP(minmn+1), WORK(m+n+1) )
	allocate( X((m+1),nb), Y((n+1),nb) )
	
	call random_number(A)
	call mkchkmat( A, lda, m, n )
	AA = A
	
	if( m+n <= 12 ) then
		write (*,*) 'Original A'
		do i=1,m+1
    		write(*,"(100F20.10)")  ( A(i,j), j=1,n+1 )
		end do	
	end if
	!dlabrd (M, N, NB, A, LDA, D, E, TAUQ, TAUP, X, LDX, Y, LDY)
	!call ft_dlabrd( m, n, nb, A, lda, D, E, TAUQ, TAUP, X, ldx, Y, ldy )
	call ft_dgebd2( m, n, a, lda, d, e, tauq, taup, work, info )
	call dgebd2( m, n, AA, lda, d, e, tauq, taup, work, info )
	if( m+n <= 12 ) then
		write (*,*) 'right after ft_dgebd2'
		write (*,*) 'A:'
		do i=1,m+1
    		write(*,"(100F12.3)")  ( A(i,j), j=1,n+1 )
		end do	
		write (*,*) 'AA:'
		do i=1,m
    		write(*,"(100F12.3)")  ( AA(i,j), j=1,n )
		end do		
	end if
	! verify the the resulting matrix A satisfy the invariant:
	write (*,*) '[m,n,nb]=',m,n,nb
	write (*,*) 'FT_DGEBD2 first iteration completed; verifyting invariant...'
	
	write (*,*) '#1 Verifying A(1:m,1:n)==AA(1:m,1:n)', MAXVAL(ABS(A(1:m,1:n)-AA(1:m,1:n)))
	write (*,*) '#2 Verifying A(m+1,n+1)'
	work(1) = A(m+1,n+1)
	do i=1,n-1
		work(1) = work(1) - A(i,i) - A(i, i+1)
	end do	
	work(1) = work(1) - A(n,n)
	
	write (*,*) work(1)
	write (*,*) '#3 Verifying checksums of V and U'
	do j=1,n
		work(j) = A(m+1,j) - SUM( A(j+1:m,j) ) - 1.0
	end do
	do i=1,n-1
		work(n+i) = A(i,n+1) - SUM( A(i, i+2:n) ) - 1.0
	end do
	write (*,*) 'MAX,LOC',MAXVAL(ABS(WORK(1:2*n-1))),MAXLOC(ABS(WORK(1:2*n-1)))
	write (*,*) 'MEAN,VAR', MEAN(ABS(WORK(1:2*n-1))), VARIANCE(ABS(WORK(1:2*n-1)))
	write (*,*) 'MIN,LOC', MINVAL(ABS(WORK(1:2*n-1))),MINLOC(ABS(WORK(1:2*n-1)))
	write (*,"(1000ES12.3)") (ABS(WORK(1:2*n-1)))
	if( m>n ) then
		write (*,*) '#4 Verifying A(n+1:m,n+1)==0'
		write (*,*) 'MAX,LOC',MAXVAL(ABS(A(n+1:m,n+1))), MAXLOC(ABS(A(n+1:m,n+1)))
		write (*,*) 'MEAN,VAR', MEAN(ABS(A(n+1:m,n+1))), VARIANCE(ABS(A(n+1:m,n+1)))
	end if
end program
subroutine mkchkmat( A, lda, m, n )
	implicit none
	integer :: m, n, lda
	double precision :: A( lda, * )

	A(m+1, 1:n) = sum( A(1:m, 1:n), 1 )
	A(1:m, n+1) = sum( A(1:m, 1:n), 2 )
	A(m+1, n+1) = sum( A(1:m, n+1) )

end subroutine

subroutine pmat2( A, lda, m, n )
double precision :: A( lda, * )
integer :: lda, m, n, i, j

do i=1,m
	write(*,"(100F12.3)") (A(i,j), j=1,n)
end do
end subroutine

function variance( A )
double precision :: A(:), variance, x
x= sum(A) / size(A)
variance = sum( (A-x)**2)/size(A)

end function

function mean(A)
double precision :: A(:), mean

mean = sum(A) / size(A)
end function