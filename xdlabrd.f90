program xdlabrd
	implicit none
	integer :: m, n, nb, lda, ldx, ldy
	double precision, allocatable :: A(:,:), D(:), E(:), X(:,:), Y(:,:)
	double precision, allocatable::  TAUQ(:), TAUP(:), WORK(:)
	integer :: argc, minmn, i, j
	character(len=31) :: arg
	double precision, parameter :: one = 1.0D+0
	double precision :: ad

	argc = command_argument_count()
	if( argc .ne. 3 ) then
		print *,'argc=', argc
		print *, 'usage: xdgebrd m n'
		call exit(1)
	end if
	
	call get_command_argument(1, arg)
	read (arg, '(I10)') m
	call get_command_argument(2, arg)
	read (arg, '(I10)') n
	call get_command_argument(3, arg)
	read (arg, '(I10)') nb
	print *,'DLABRD:m,n=',m,n
	minmn = min(m, n)
	lda = m+1
	ldx = m+1
	ldy = n+1
	!nb = 2

	allocate( A(m+1,n+1) )
	allocate( D(minmn), E(minmn), TAUQ(minmn), TAUP(minmn), WORK((1+max(m,n))*nb) )
	allocate( X((m+1),nb), Y((n+1),nb) )
	
	do i=1,m
		do j=1,n
			a( i, j ) = i+2*j
		end do
	end do
	call random_number(A)
	call mkchkmat( A, lda, m, n )
	
	if( m+n <= 12 ) then
		write (*,*) 'Original A'
		do i=1,m+1
    		write(*,"(100F20.10)")  ( A(i,j), j=1,n+1 )
		end do	
	end if
	!dlabrd (M, N, NB, A, LDA, D, E, TAUQ, TAUP, X, LDX, Y, LDY)
	call ft_dlabrd( m, n, nb, A, lda, D, E, TAUQ, TAUP, X, ldx, Y, ldy )
	if( m+n <= 12 ) then
		write (*,*) 'right after ft_dlabrd'
		write (*,*) 'A:'
		do i=1,m+1
    		write(*,"(100F12.3)")  ( A(i,j), j=1,n+1 )
		end do	
		write (*,*) 'X:'
		do i =1,m+1
			write(*,"(100F12.3)")  ( X(i,j), j=1,nb )
		end do
		write (*,*) 'Y:'
		do i =1,n+1
			write(*,"(100F12.3)")  ( Y(i,j), j=1,nb )
		end do
	end if
	call dgemm( 'No transpose', 'Transpose', m-nb + 1, n-nb + 1, nb, &
				-one, A( 1+nb, 1), lda, Y(nb+1,1), ldy, one, A(1+nb, 1+nb), lda )
	call dgemm( 'No transpose', 'No transpose', m-nb + 1, n-nb + 1, nb, &
				-one, X(nb+1,1), ldx, A(1, 1+nb), lda, one, A(1+nb,1+nb), lda )
	! verify the the resulting matrix A satisfy the invariant:
	write (*,*) '[m,n,nb]=',m,n,nb
	write (*,*) 'FT_DLABRD first iteration completed; verifyting invariant...'
	
	write (*,*) 'Verifying row checksums...'
	if( m+n <= 12 ) then
		do, i=1,m+1
    		write(*,"(100F12.3)") ( A(i,j), j=1,n+1 )
		end do
	end if		

	write (*,*) 'Checking V...'
	ad = 0
	do j=1,nb
		ad = ad + abs( sum(a(j:m, j)) - a(m+1, j) )
	end do
	write (*,*) 'sum of abs diff', ad
	
	write (*,*) 'Checking U...'
	ad = 0
	do i=1,nb
		ad = ad + abs( sum(a(i, i+1:n)) - a(i, n+1) )
	end do
	write (*,*) 'sum of abs diff', ad	
	
	write (*,*) 'Checking the trailing matrix...'
	ad = 0
	do i=nb+1,m
		ad = ad + abs( sum(a(i,nb+1:n)) - a(i, n+1) )
	end do
	write (*,*) 'row sum of abs diff', ad
	ad = 0
	ad = abs( E(nb) +  sum(a(nb+1:m, nb+1)) - a(m+1, nb+1) )
	write (*,*) 'first column diff', ad, 'E(nb)', E(nb)
	do j=nb+2,n
		ad = ad + abs( sum(a(nb+1:m, j)) - a(m+1,j) )
	end do
	write (*,*) 'column sum of abs diff', ad	
	write (*,*) 'checking a(m+1,n+1) == D(1:nb) + E(1:nb-1)'
	write (*,*) 'abs( a(m+1,n+1) - sum(D(1:nb)) - sum( E(1:nb-1) )', &
		abs( a(m+1,n+1) - sum(D(1:nb)) - sum( E(1:nb-1) ) - sum( A(m+1, nb+1:n) ) )	
end program
subroutine mkchkmat( A, lda, m, n )
	implicit none
	integer :: m, n, lda
	double precision :: A( lda, * )

	A(m+1, 1:n) = sum( A(1:m, 1:n), 1 )
	A(1:m, n+1) = sum( A(1:m, 1:n), 2 )
	A(m+1, n+1) = sum( A(1:m, n+1) )

end subroutine