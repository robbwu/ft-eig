program xft2_dgemm
  implicit none
  integer m, n, lda
  double precision one, zero

  parameter (m=500, n=500, one=1.0D0, zero=0.0D0)
  double precision A(m+2, m+2), B(m+2, m+2) , C(m+2, m+2) 
  lda = m+2

  call random_number(A)
  call random_number(B)
  call random_number(C)
  call ft2_dgemm('N', 'N', m, m, m, one, A, lda, B, lda, -one, C, lda)
  call ft2_dgemm('N', 'T', m, m, m, one, A, lda, B, lda, -one, C, lda)
end program xft2_dgemm
