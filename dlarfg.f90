!> \brief \b DLARFG generates an elementary reflector (Householder matrix).

! Code converted using TO_F90 by Alan Miller
! Date: 2016-02-19  Time: 16:04:30

!  =========== DOCUMENTATION ===========

! Online html documentation available at
!            http://www.netlib.org/lapack/explore-html/

!> \htmlonly
!> Download DLARFG + dependencies
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.tgz?format=tgz&filename=/lapack/lapack_routine/dlarfg.f">
!> [TGZ]</a>
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.zip?format=zip&filename=/lapack/lapack_routine/dlarfg.f">
!> [ZIP]</a>
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.txt?format=txt&filename=/lapack/lapack_routine/dlarfg.f">
!> [TXT]</a>
!> \endhtmlonly

!  Definition:
!  ===========

!       SUBROUTINE DLARFG( N, ALPHA, X, INCX, TAU )

!       .. Scalar Arguments ..
!       INTEGER            INCX, N
!       DOUBLE PRECISION   ALPHA, TAU
!       ..
!       .. Array Arguments ..
!       DOUBLE PRECISION   X( * )
!       ..


!> \par Purpose:
!  =============
!>
!> \verbatim
!>
!> DLARFG generates a real elementary reflector H of order n, such
!> that
!>
!>       H * ( alpha ) = ( beta ),   H**T * H = I.
!>           (   x   )   (   0  )
!>
!> where alpha and beta are scalars, and x is an (n-1)-element real
!> vector. H is represented in the form
!>
!>       H = I - tau * ( 1 ) * ( 1 v**T ) ,
!>                     ( v )
!>
!> where tau is a real scalar and v is a real (n-1)-element
!> vector.
!>
!> If the elements of x are all zero, then tau = 0 and H is taken to be
!> the unit matrix.
!>
!> Otherwise  1 <= tau <= 2.
!> \endverbatim

!  Arguments:
!  ==========

!> \param[in] N
!> \verbatim
!>          N is INTEGER
!>          The order of the elementary reflector.
!> \endverbatim
!>
!> \param[in,out] ALPHA
!> \verbatim
!>          ALPHA is DOUBLE PRECISION
!>          On entry, the value alpha.
!>          On exit, it is overwritten with the value beta.
!> \endverbatim
!>
!> \param[in,out] X
!> \verbatim
!>          X is DOUBLE PRECISION array, dimension
!>                         (1+(N-2)*abs(INCX))
!>          On entry, the vector x.
!>          On exit, it is overwritten with the vector v.
!> \endverbatim
!>
!> \param[in] INCX
!> \verbatim
!>          INCX is INTEGER
!>          The increment between elements of X. INCX > 0.
!> \endverbatim
!>
!> \param[out] TAU
!> \verbatim
!>          TAU is DOUBLE PRECISION
!>          The value tau.
!> \endverbatim

!  Authors:
!  ========

!> \author Univ. of Tennessee
!> \author Univ. of California Berkeley
!> \author Univ. of Colorado Denver
!> \author NAG Ltd.

!> \date September 2012

!> \ingroup doubleOTHERauxiliary

!  =====================================================================

SUBROUTINE ft_dlarfg( n, alpha, x, incx, tau )

!  -- LAPACK auxiliary routine (version 3.4.2) --
!  -- LAPACK is a software package provided by Univ. of Tennessee,    --
!  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
!     September 2012

!     .. Scalar Arguments ..

INTEGER, INTENT(IN)                      :: n
DOUBLE PRECISION, INTENT(IN OUT)         :: alpha
DOUBLE PRECISION, INTENT(IN)             :: x( * )
INTEGER, INTENT(IN)                      :: incx
DOUBLE PRECISION, INTENT(OUT)            :: tau


!     ..
!     .. Array Arguments ..

!     ..

!  =====================================================================

!     .. Parameters ..

DOUBLE PRECISION, PARAMETER :: one = 1.0D+0
DOUBLE PRECISION, PARAMETER :: zero = 0.0D+0
!     ..
!     .. Local Scalars ..
INTEGER :: j, knt
DOUBLE PRECISION :: beta, rsafmn, safmin, xnorm
!     ..
!     .. External Functions ..
DOUBLE PRECISION :: dlamch, dlapy2, dnrm2
EXTERNAL           dlamch, dlapy2, dnrm2
!     ..
!     .. Intrinsic Functions ..
INTRINSIC          ABS, SIGN
!     ..
!     .. External Subroutines ..
EXTERNAL           dscal
!     ..
!     .. Executable Statements ..

IF( n <= 1 ) THEN
  tau = zero
  RETURN
END IF

xnorm = dnrm2( n-1, x, incx )

IF( xnorm == zero ) THEN
  
!        H  =  I
  
  tau = zero
ELSE
  
!        general case
  
  beta = -SIGN( dlapy2( alpha, xnorm ), alpha )
  safmin = dlamch( 'S' ) / dlamch( 'E' )
  knt = 0
  IF( ABS( beta ) < safmin ) THEN
    
!           XNORM, BETA may be inaccurate; scale X and recompute them
    
    rsafmn = one / safmin
    10       CONTINUE
    knt = knt + 1
    CALL dscal( n-1, rsafmn, x, incx )
    beta = beta*rsafmn
    alpha = alpha*rsafmn
    IF( ABS( beta ) < safmin ) GO TO 10
    
!           New BETA is at most 1, at least SAFMIN
    
    xnorm = dnrm2( n-1, x, incx )
    beta = -SIGN( dlapy2( alpha, xnorm ), alpha )
  END IF
  tau = ( beta-alpha ) / beta
!  CALL dscal( n-1, one / ( alpha-beta ), x, incx )
  CALL dscal( n, one / (alpha - beta), x, incx )
  
!        If ALPHA is subnormal, it may lose relative accuracy
  
  DO  j = 1, knt
    beta = beta*safmin
  END DO
  alpha = beta
END IF

RETURN

!     End of DLARFG

END SUBROUTINE ft_dlarfg
