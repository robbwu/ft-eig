program xdgebrd
  ! this program tests the fault-tolerant bidiagonalization subroutine
  ! dgebrd().
  implicit none

  double precision, allocatable:: A(:,:),A2(:,:), D(:), E(:), TAUQ(:), TAUP(:), WORK(:)
  double precision, allocatable :: D2(:), E2(:)
  integer :: argc, m, n, lda, minmn, info, lwork, nb, lda2, nx
  character(len=31) :: arg

  double precision :: dlange
  double precision :: n2
  integer :: i, j, exec
  INTEGER :: ilaenv
  EXTERNAL           ilaenv
  real :: start, end
  double precision :: eps, diff
  parameter (eps=1.0D-3)

  argc = command_argument_count()
  if( argc .ne. 3 ) then
     print *,'argc=', argc
     print *, 'usage: xdgebrd FT{1,2,3} m n'
     call exit(1)
  end if
  call get_command_argument(1, arg)
  read (arg, '(I10)') exec
  call get_command_argument(2, arg)
  read (arg, '(I10)') m
  call get_command_argument(3, arg)
  read (arg, '(I10)') n

  !print *, 'matrix size', m, n
  minmn = min( m, n )
  lda = m + 2
  lda2 = m + 2

  nb = MAX( 1, ilaenv( 1, 'DGEBRD', ' ', m, n, -1, -1 ) )
  nx = MAX( nb, ilaenv( 3, 'DGEBRD', ' ', m, n, -1, -1 ) )

  print *,'NB=', nb, 'NX=',nx
  allocate( A(m+2,n+2), A2(m+2,n+2) )
  allocate( D(minmn+1),D2(minmn+1) )
  allocate( E(minmn+1),E2(minmn+1) )
  allocate( TAUQ(minmn+1) )
  allocate( TAUP(minmn+1) )
  allocate( WORK((m+n+2+2)*nb) )
  lwork = (m+n+2+2) * nb;

  call random_number( A )

  A2 = A
  call mkchkmat( A, lda, m, n )
  !call ft_dgebd2( m, n, A, lda, D, E, TAUQ, TAUP, WORK, info )


  print *,'NORM_F(A)=', dlange( 'F', m, n, A, lda, WORK )
  print *, 'ft_dgebrd begin'
  call cpu_time(start)
  if( exec .eq. 1 ) then
     call ft_dgebrd( m, n, A, lda, D, E, TAUQ, TAUP, WORK, lwork, info )
  else if( exec .eq. 2 ) then
     call ft2_dgebrd( m, n, A, lda, D, E, TAUQ, TAUP, WORK, lwork, info )
  else if (exec .eq. 3) then
     call ft3_dgebrd( m, n, A, lda, D, E, TAUQ, TAUP, WORK, lwork, info )
  end if
  call cpu_time(end)
  write (*,*) 'ft_dgebrd time(s):', end-start
  call cpu_time(start)
  call dgebrd( m, n, A2, lda2, D2, E2, TAUQ, TAUP, WORK, lwork, info )
  call cpu_time(end)
  write (*,*) 'dgebrd time(s):', end-start
  n2 = 0
  do i=1,n-1
     n2 = n2 + D(i)*D(i) + E(i)*E(i)
  end do
  n2 = n2 + D(n) * D(n)
  n2 = sqrt(n2)
  print *,'NORM_F(B)=', n2

  diff =  MAXVAL(ABS(A(1:m,1:n)-A2(1:m,1:n)))
  print *, 'MAXVAL(ABS(A-A2))=',diff
  if( diff < EPS ) then
     print *, '### XDGEBRD SUCCESS'
  else
     
     print *, '### XDGEBRD FAILURE'
  end if
  
  print *, 'MAXVAL(ABS(D-D2))=', MAXVAL(ABS(D-D2))
  print *, 'MAXVAL(ABS(E(1:n-1)-E2(1:n-1)))', MAXVAL(ABS(E(1:n-1)-E2(1:n-1)))
  !call vrfchs( A, lda, m, n )
  write (*,*) '[m,n,nb]=',m,n,nb
  write (*,*) 'FT_DGEBRD  completed; verifyting invariant...'


  write (*,*) '#2 Verifying A(m+1,n+1)'
  work(1) = A(m+1,n+1)
  do i=1,n-1
     work(1) = work(1) - D(i) - E(i)
  end do
  work(1) = work(1) - D(n)

  write (*,*) work(1)
  write (*,*) '#3 Verifying checksums of V and U'
  do j=1,n
     work(j) = A(m+1,j) - SUM( A(j+1:m,j) ) - 1.0
  end do
  do i=1,n-1
     work(n+i) = A(i,n+1) - SUM( A(i, i+2:n) ) - 1.0
  end do
  write (*,*) 'MAX,LOC',MAXVAL(ABS(WORK(1:2*n-1))),MAXLOC(ABS(WORK(1:2*n-1)))
  !write (*,*) 'MEAN,VAR', MEAN(ABS(WORK(1:2*n-1))), VARIANCE(ABS(WORK(1:2*n-1)))
  write (*,*) 'MIN,LOC', MINVAL(ABS(WORK(1:2*n-1))),MINLOC(ABS(WORK(1:2*n-1)))
  !write (*,"(1000ES12.3)") (ABS(WORK(1:2*n-1)))
  if( m>n ) then
     write (*,*) '#4 Verifying A(n+1:m,n+1)==0'
     write (*,*) 'MAX,LOC',MAXVAL(ABS(A(n+1:m,n+1))), MAXLOC(ABS(A(n+1:m,n+1)))
     !write (*,*) 'MEAN,VAR', MEAN(ABS(A(n+1:m,n+1))), VARIANCE(ABS(A(n+1:m,n+1)))
  end if

end program xdgebrd

subroutine mkchkmat( A, lda, m, n )
  implicit none
  integer :: m, n, lda, i, j
  double precision :: A( lda, * ), w( max(m,n) )



  A(m+1, 1:n) = sum( A(1:m, 1:n), 1 )
  A(m+2,1:n) = 0
  do j=1,n
     do i=1,m
        A(m+2,j) = A(m+2, j) + A(i,j) * i
     end do
  end do

  A(1:m, n+1) = sum( A(1:m, 1:n), 2 )
  A(1:m, n+2) = 0
  do i=1,m
     do j=1,n
        A(i, n+2) = A(i, n+2) + A(i,j) * j
     end do
  end do

  A(m+1,n+1) = sum( A(1:m, n+1) )

end subroutine

subroutine vrfchs( A, lda, m, n )
	implicit none
	integer :: m, n, lda
	double precision :: A( lda, * )

	integer :: i, j
	double precision :: s1, d, eps, dr( n ), dc( m )

	eps = 1.0D-6

	do j=1,n
		d = sum(  A(j+1:m, j) ) - A(m+1, j)
		dr( j ) = d
	end do
	do i=1,m
		d = sum( A(i, i+1:n) ) - A(i, n+1)
		dc( i ) = d
	end do
	print *, 'max diff in column/row', maxval(abs(dr)), maxval(abs(dc)), maxloc(abs(dr)), maxloc(abs(dc))
	if (m .LE. 10 .AND. n .LE. 10 ) then
		print *, 'dr',dr
		print *, 'dc',dc
	end if
end subroutine
