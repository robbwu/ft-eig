import pexpect
import re
import random
import time
import sys

if len(sys.argv) != 5:
	print 'Usage', sys.argv[0], '<exec path>', '<start index>', '<end index>', '<estimate exec time>'
	sys.exit(1)
else:
        execpath = sys.argv[1]
        starti = int(sys.argv[2])
	endi= int(sys.argv[3])
	exectime = float(sys.argv[4])


child = pexpect.spawn('gdb --args ' +execpath +' 1000 1000')
child.expect_exact( '(gdb) ' )
child.sendline('display/i $pc')
child.expect_exact( '(gdb) ' )
child.sendline('set disassembly-flavor intel')
child.expect_exact( '(gdb) ' )

for i in range(starti,endi):
	print 'Run #'+str(i)
	with open(str(i)+'.log', 'w') as f:
		delay = random.random() * exectime

		child.sendline('run')
		child.expect('ft_dgebrd begin')
                print 'after run:', child.before
                time.sleep(delay)
                child.sendcontrol('c')
                child.expect_exact( '(gdb) ' )
                print 'after C-c', child.before

                dice = random.choice([0,1])


                if dice == 0:
        # corrupt memory
                    child.sendline('bt')
                    child.expect_exact( '(gdb) ' )
                    match = re.search('#(\d+).*xdgebrd', child.before)
                    print >>f, child.before
                    if match:
                        fr = match.group(1)
                        child.sendline('fr '+str(fr) )
                        child.expect_exact( '(gdb) ' )
                        print >>f, child.before
                        m = random.randint(1, 500)
                        n = random.randint(1,500)
                        cmd = 'p A(' + str(m) + ',' + str(n) + ')=234'
                        child.sendline(cmd);
                        child.expect_exact( '(gdb) ' )
                        print >>f, child.before
                else:
        # loop through to find a floating point instruction to corrupt
                    cnt = 0
                    while True:
                        cnt = cnt + 1
                        if cnt > 200:
                            print >>f, 'cnt exceeds 200, abort injection.'
                            break
                        child.sendline('stepi')
                        child.expect_exact( '(gdb) ' )
                        lastline = child.before.split('\n')[-2]
                        print 'lastline:', lastline
                        h = re.compile( 'xmm\d{1,2}|ymm\d{1,2}' )
                        match = h.search( lastline)
                        if match:
                            print >> f, 'Instruction to corrupt:'
                            print >> f, lastline
                            fpreg = match.group(0)
                            child.sendline('bt')
                            child.expect_exact( '(gdb) ' )
                            print >> f,'Backtrace of the injection site'
                            print >> f,child.before
                            child.sendline('stepi')
                            child.expect_exact( '(gdb) ' )
                            # child.sendline('set $st0=2')
                            if fpreg.startswith('ymm'):
                                sline = 'set $' + fpreg + '.v4_double[0]=2'
                            else:
                                sline = 'set $' + fpreg + '.v2_double[0]=2'
                            print>> f, sline
                            child.sendline(sline)
                            child.expect_exact( '(gdb) ' )
                            break


                child.sendline('cont')
                child.expect_exact( '(gdb) ' )
                print >> f,child.before
