! Code converted using TO_F90 by Alan Miller
! Date: 2016-02-16  Time: 16:09:27

!  =========== DOCUMENTATION ===========

! Online html documentation available at
!            http://www.netlib.org/lapack/explore-html/
!  Definition:
!  ===========

!       SUBROUTINE FT_DGEBRD( M, N, A, LDA, D, E, TAUQ, TAUP, WORK, LWORK,
!                          INFO )

!       .. Scalar Arguments ..
!       INTEGER            INFO, LDA, LWORK, M, N
!       ..
!       .. Array Arguments ..
!       DOUBLE PRECISION   A( LDA, * ), D( * ), E( * ), TAUP( * ),
!      $                   TAUQ( * ), WORK( * )
!       ..


!> \par Purpose:
!  =============
!>
!> \verbatim
!>
!> DGEBRD reduces a general real M-by-N matrix A to upper or lower
!> bidiagonal form B by an orthogonal transformation: Q**T * A * P = B.
!>
!> If m >= n, B is upper bidiagonal; if m < n, B is lower bidiagonal.
!> \endverbatim

!  Arguments:
!  ==========

!> \param[in] M
!> \verbatim
!>          M is INTEGER
!>          The number of rows in the matrix A.  M >= 0.
!> \endverbatim
!>
!> \param[in] N
!> \verbatim
!>          N is INTEGER
!>          The number of columns in the matrix A.  N >= 0.
!> \endverbatim
!>
!> \param[in,out] A
!> \verbatim
!>          A is DOUBLE PRECISION array, dimension (LDA,N)
!>          On entry, the M-by-N general matrix to be reduced.
!>          On exit,
!>          if m >= n, the diagonal and the first superdiagonal are
!>            overwritten with the upper bidiagonal matrix B; the
!>            elements below the diagonal, with the array TAUQ, represent
!>            the orthogonal matrix Q as a product of elementary
!>            reflectors, and the elements above the first superdiagonal,
!>            with the array TAUP, represent the orthogonal matrix P as
!>            a product of elementary reflectors;
!>          if m < n, the diagonal and the first subdiagonal are
!>            overwritten with the lower bidiagonal matrix B; the
!>            elements below the first subdiagonal, with the array TAUQ,
!>            represent the orthogonal matrix Q as a product of
!>            elementary reflectors, and the elements above the diagonal,
!>            with the array TAUP, represent the orthogonal matrix P as
!>            a product of elementary reflectors.
!>          See Further Details.
!> \endverbatim
!>
!> \param[in] LDA
!> \verbatim
!>          LDA is INTEGER
!>          The leading dimension of the array A.  LDA >= max(1,M).
!> \endverbatim
!>
!> \param[out] D
!> \verbatim
!>          D is DOUBLE PRECISION array, dimension (min(M,N))
!>          The diagonal elements of the bidiagonal matrix B:
!>          D(i) = A(i,i).
!> \endverbatim
!>
!> \param[out] E
!> \verbatim
!>          E is DOUBLE PRECISION array, dimension (min(M,N)-1)
!>          The off-diagonal elements of the bidiagonal matrix B:
!>          if m >= n, E(i) = A(i,i+1) for i = 1,2,...,n-1;
!>          if m < n, E(i) = A(i+1,i) for i = 1,2,...,m-1.
!> \endverbatim
!>
!> \param[out] TAUQ
!> \verbatim
!>          TAUQ is DOUBLE PRECISION array dimension (min(M,N))
!>          The scalar factors of the elementary reflectors which
!>          represent the orthogonal matrix Q. See Further Details.
!> \endverbatim
!>
!> \param[out] TAUP
!> \verbatim
!>          TAUP is DOUBLE PRECISION array, dimension (min(M,N))
!>          The scalar factors of the elementary reflectors which
!>          represent the orthogonal matrix P. See Further Details.
!> \endverbatim
!>
!> \param[out] WORK
!> \verbatim
!>          WORK is DOUBLE PRECISION array, dimension (MAX(1,LWORK))
!>          On exit, if INFO = 0, WORK(1) returns the optimal LWORK.
!> \endverbatim
!>
!> \param[in] LWORK
!> \verbatim
!>          LWORK is INTEGER
!>          The length of the array WORK.  LWORK >= max(1,M,N).
!>          For optimum performance LWORK >= (M+N)*NB, where NB
!>          is the optimal blocksize.
!>
!>          If LWORK = -1, then a workspace query is assumed; the routine
!>          only calculates the optimal size of the WORK array, returns
!>          this value as the first entry of the WORK array, and no error
!>          message related to LWORK is issued by XERBLA.
!> \endverbatim
!>
!> \param[out] INFO
!> \verbatim
!>          INFO is INTEGER
!>          = 0:  successful exit
!>          < 0:  if INFO = -i, the i-th argument had an illegal value.
!> \endverbatim

!  Authors:
!  ========

!> \author Univ. of Tennessee
!> \author Univ. of California Berkeley
!> \author Univ. of Colorado Denver
!> \author NAG Ltd.

!> \date November 2011

!> \ingroup doubleGEcomputational

!> \par Further Details:
!  =====================
!>
!> \verbatim
!>
!>  The matrices Q and P are represented as products of elementary
!>  reflectors:
!>
!>  If m >= n,
!>
!>     Q = H(1) H(2) . . . H(n)  and  P = G(1) G(2) . . . G(n-1)
!>
!>  Each H(i) and G(i) has the form:
!>
!>     H(i) = I - tauq * v * v**T  and G(i) = I - taup * u * u**T
!>
!>  where tauq and taup are real scalars, and v and u are real vectors;
!>  v(1:i-1) = 0, v(i) = 1, and v(i+1:m) is stored on exit in A(i+1:m,i);
!>  u(1:i) = 0, u(i+1) = 1, and u(i+2:n) is stored on exit in A(i,i+2:n);
!>  tauq is stored in TAUQ(i) and taup in TAUP(i).
!>
!>  If m < n,
!>
!>     Q = H(1) H(2) . . . H(m-1)  and  P = G(1) G(2) . . . G(m)
!>
!>  Each H(i) and G(i) has the form:
!>
!>     H(i) = I - tauq * v * v**T  and G(i) = I - taup * u * u**T
!>
!>  where tauq and taup are real scalars, and v and u are real vectors;
!>  v(1:i) = 0, v(i+1) = 1, and v(i+2:m) is stored on exit in A(i+2:m,i);
!>  u(1:i-1) = 0, u(i) = 1, and u(i+1:n) is stored on exit in A(i,i+1:n);
!>  tauq is stored in TAUQ(i) and taup in TAUP(i).
!>
!>  The contents of A on exit are illustrated by the following examples:
!>
!>  m = 6 and n = 5 (m > n):          m = 5 and n = 6 (m < n):
!>
!>    (  d   e   u1  u1  u1 )           (  d   u1  u1  u1  u1  u1 )
!>    (  v1  d   e   u2  u2 )           (  e   d   u2  u2  u2  u2 )
!>    (  v1  v2  d   e   u3 )           (  v1  e   d   u3  u3  u3 )
!>    (  v1  v2  v3  d   e  )           (  v1  v2  e   d   u4  u4 )
!>    (  v1  v2  v3  v4  d  )           (  v1  v2  v3  e   d   u5 )
!>    (  v1  v2  v3  v4  v5 )
!>
!>  where d and e denote diagonal and off-diagonal elements of B, vi
!>  denotes an element of the vector defining H(i), and ui an element of
!>  the vector defining G(i).
!> \endverbatim
!>
!  =====================================================================

SUBROUTINE ft_dgebrd( m, n, a, lda, d, e, tauq, taup, work, lwork, info )

!  -- LAPACK computational routine (version 3.4.0) --
!  -- LAPACK is a software package provided by Univ. of Tennessee,    --
!  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
!     November 2011

!     .. Scalar Arguments ..

INTEGER, INTENT(IN)                      :: m
INTEGER, INTENT(IN)                      :: n
DOUBLE PRECISION, INTENT(OUT)            :: a( lda, * )
INTEGER, INTENT(IN OUT)                  :: lda
DOUBLE PRECISION, INTENT(IN)             :: d( * )
DOUBLE PRECISION, INTENT(IN)             :: e( * )
DOUBLE PRECISION, INTENT(IN OUT)         :: tauq( * )
DOUBLE PRECISION, INTENT(IN OUT)         :: taup( * )
DOUBLE PRECISION, INTENT(OUT)            :: work( * )
INTEGER, INTENT(IN)                      :: lwork
INTEGER, INTENT(OUT)                     :: info

!     ..
!     .. Array Arguments ..

!     ..

!  =====================================================================

!     .. Parameters ..

DOUBLE PRECISION, PARAMETER :: one = 1.0D+0
!     ..
!     .. Local Scalars ..
LOGICAL :: lquery
INTEGER :: i, iinfo, j, ldwrkx, ldwrky, lwkopt, minmn, nb, nbmin, nx
DOUBLE PRECISION :: ws
!     ..
!     .. External Subroutines ..
EXTERNAL           dgebd2, dgemm, ft_dlabrd, xerbla
!     ..
!     .. Intrinsic Functions ..
INTRINSIC          DBLE, MAX, MIN
!     ..
!     .. External Functions ..
DOUBLE PRECISION :: dlange, fnorm, SUMM
INTEGER :: ilaenv
EXTERNAL           ilaenv, dlange
!     .. Checksum ..
DOUBLE PRECISION  w(max(m,n))
DOUBLE PRECISION, ALLOCATABLE :: A1(:), A2(:)
!     ..
!     .. Executable Statements ..

!     Test the input parameters

info = 0
nb = MAX( 1, ilaenv( 1, 'DGEBRD', ' ', m, n, -1, -1 ) )
lwkopt = ( m+n+2+2 )*nb
work( 1 ) = DBLE( lwkopt )
lquery = ( lwork == -1 )
IF( m < 0 ) THEN
  info = -1
ELSE IF( n < 0 ) THEN
  info = -2
ELSE IF( lda < MAX( 1, m ) ) THEN
  info = -4
ELSE IF( lwork < MAX( 1, m, n ) .AND. .NOT.lquery ) THEN
  info = -10
END IF
IF( info < 0 ) THEN
  CALL xerbla( 'DGEBRD', -info )
  RETURN
ELSE IF( lquery ) THEN
  RETURN
END IF

!     Quick return if possible

minmn = MIN( m, n )
IF( minmn == 0 ) THEN
  work( 1 ) = 1
  RETURN
END IF

ws = MAX( m, n )
ldwrkx = m + 2
ldwrky = n + 2

do i=1,max(m,n)
   w( i ) = i
end do


IF( nb > 1 .AND. nb < minmn ) THEN

!        Set the crossover point NX.

  nx = MAX( nb, ilaenv( 3, 'DGEBRD', ' ', m, n, -1, -1 ) )

!        Determine when to switch from blocked to unblocked code.

  IF( nx < minmn ) THEN
    ws = ( m+n+2+2 )*nb
    IF( lwork < ws ) THEN
       PRINT *, 'Not enough work space for optimal NB. Looking for smaller block size'

!              Not enough work space for the optimal NB, consider using
!              a smaller block size.

      nbmin = ilaenv( 2, 'DGEBRD', ' ', m, n, -1, -1 )
      IF( lwork >= ( m+n )*nbmin ) THEN
        nb = lwork / ( m+n )
      ELSE
        nb = 1
        nx = minmn
      END IF
    END IF
  END IF
ELSE
  nx = minmn
END IF

ALLOCATE( A1((m+2)*nb), A2(nb*(n+2)) )

DO  i = 1, minmn - nx, nb
   info = 0
   CALL dlacpy( 'All', m-i+1+2, nb, a( i, i ), lda, A1, m-i+1+2 )
   CALL dlacpy( 'All', nb, n-i+1+2, a( i, i ), lda, A2, nb )
!        Reduce rows and columns i:i+nb-1 to bidiagonal form and return
!        the matrices X and Y which are needed to update the unreduced
!        part of the matrix
  CALL ft_dlabrd( m-i+1, n-i+1, nb, a( i, i ), lda, d( i ), e( i ),  &
       tauq( i ), taup( i ), work, ldwrkx, work( ldwrkx*nb+1 ), ldwrky,&
       w(i), info )
  if( info.lt.0 )  then       !
     print *, '[warn] dgebrd: error detected in ft_dlabrd. rollback.'
     CALL dlacpy( 'All', m-i+1+2, nb, A1, m-i+1+2, a( i, i ), lda )
     CALL dlacpy( 'All', nb, n-i+1+2, A2, nb, a( i, i ), lda )
     CALL ft_dlabrd( m-i+1, n-i+1, nb, a( i, i ), lda, d( i ), e( i ),  &
          tauq( i ), taup( i ), work, ldwrkx, work( ldwrkx*nb+1 ), ldwrky,&
          w(i), info )
     if( info.lt.0 ) then
        print *,'rollback failed. abort'
        return
     end if

  end if
!        Update the trailing submatrix A(i+nb:m,i+nb:n), using an update
!        of the form  A := A - V*Y**T - X*U**T

  ! Inject an memory corruption before DGEMM
  ! IF( i.eq.1 ) A(nb+2,2) = 8
  CALL dgemm( 'No transpose', 'Transpose', m-i-nb+1+2, n-i-nb+1+2,  &
      nb, -one, a( i+nb, i ), lda, work( ldwrkx*nb+nb+1 ), ldwrky, one,  &
      a( i+nb, i+nb ), lda )
  CALL dgemm( 'No transpose', 'No transpose', m-i-nb+1+2, n-i-nb+1+2,  &
      nb, -one, work( nb+1 ), ldwrkx, a( i, i+nb ), lda,  &
      one, a( i+nb, i+nb ), lda )

!        Copy diagonal and off-diagonal elements of B back into A

  IF( m >= n ) THEN
    DO  j = i, i + nb - 1
      a( j, j ) = d( j )
      a( j, j+1 ) = e( j )
    END DO
  ELSE
    DO  j = i, i + nb - 1
      a( j, j ) = d( j )
      a( j+1, j ) = e( j )
    END DO
  END IF

!  IF( MOD( i-1, 128 ) .EQ. 0 ) FNORM = SUMM(  m-i, n-i, A(i,i), lda )
  ! CALL CHK1( A( i+nb, i+nb ), lda, m-i-nb+1, n-i-nb+1,  nb, D, E )
  CALL CHK1_VEC( A( i+nb, i+nb ), lda, m-i-nb+1, n-i-nb+1, w(i+nb) )
END DO

!     Use unblocked code to reduce the remainder of the matrix
!	Adjust the checksum;
IF( i>1 ) THEN
  A(m+1, i) = A(m+1,i) - A(i-1,i)
  !A(m+1,n+1) = A(m+1, n+1)
END IF
CALL ft_dgebd2( m-i+1, n-i+1, a( i, i ), lda, d( i ), e( i ),  &
    tauq( i ), taup( i ), work, iinfo )
work( 1 ) = ws

! Brush the whole matrix for memory error.
CALL CHK2( M, N, A, LDA, W, i )
RETURN

!     End of DGEBRD

END SUBROUTINE ft_dgebrd

SUBROUTINE CHK2( M, N, A, LDA, W, LAST )
  integer M, N, LDA
  double precision A(LDA, *), W(*)

  integer i, j, last

  do i=1,last-1
     A(m+1, i) = A(m+1,i) - 1.0
     A(m+2, i) = A(m+2,i) - 1.0*w(i)
     call chkvec( m-i, A(i+1,i), 1, w(i+1) )
  end do

  do i=1,last-2
     A(i, n+1) = A(i, n+1) - 1.0
     A(i, n+2) = A(i, n+2) - 1.0*w(i+1)
     call chkvec( n-i-1, A(i, i+2), lda, w(i+2) )
  end do

END SUBROUTINE CHK2

! Verify the trailing matrix
SUBROUTINE CHK1_VEC( A, lda, m, n, w )
double precision :: A( lda, * ), w( * )
integer :: m, n, lda, i, nb
double precision :: dnrm2


call chkvec( m+1, A(0,1), 1, w(0) )
do i=2,n
   call chkvec( m, A(1,i), 1, w)
end do

do i=1,m
   call chkvec( n, A(i,1), lda, w)
end do

END SUBROUTINE CHK1_VEC

SUBROUTINE CHK1(  A, lda, m, n, nb, D, E )
double precision :: A( lda, * ), D( * ), E( * )
integer :: m, n, lda, i, nb, j
double precision :: RD( n ), CD( m )
double precision :: dnrm2


CD(1:m) = -A(1:m,n+1)
RD(1:n) = -A(m+1,1:n)
do i=n,1,-1
	do j=1,m
		CD(i) = CD(i) + A(i,j)
		RD(j) = RD(j) + A(i,j)
	end do
end do
RD(1) = RD(1) + A(0,1)

WRITE (*,*) 'RD: MAX, LOC', MAXVAL(ABS(RD)), MAXLOC(ABS(RD)),'NORM', DNRM2(N, RD, 1)
WRITE (*,*) 'CD: MAX, LOC', MAXVAL(ABS(CD)), MAXLOC(ABS(CD)),'NORM', DNRM2(M, CD, 1)
WRITE (*,*) 'A(m+1,n+1)-SUM(E) -SUM(D)',  &
	ABS( A(m+1,n+1) - SUM(D(1:i-1+nb)) - SUM(E(1:i-2+nb)) - SUM( A(m+1, 1:n) ) )
END SUBROUTINE CHK1

SUBROUTINE CHK1_OPT(  A, lda, m, n, nb, D, E )
double precision :: A( lda, * ), D( * ), E( * )
integer :: m, n, lda, i, nb, j
double precision :: RD( n ), CD( m )
double precision :: dnrm2


!RD(1:n) = SUM( A(1:m, 1:n) , 1 ) - A(m+1, 1:n)
!RD(1) = RD(1) + A(0, 1)
!CD(1:m) = SUM( A(1:m, 1:n), 2 ) - A(1:m, n+1)
CD(1:m) = -A(1:m,n+1)
RD(1:n) = -A(m+1,1:n)
do i=n,1,-1
	do j=1,m
		CD(i) = CD(i) + A(i,j)
		RD(j) = RD(j) + A(i,j)
	end do
end do

!WRITE (*,*) 'RD: MAX, LOC', MAXVAL(ABS(RD)), 'NORM', DNRM2(N, RD, 1)
!WRITE (*,*) 'CD: MAX, LOC', MAXVAL(ABS(CD)), 'NORM', DNRM2(M, CD, 1)
!WRITE (*,*) 'A(m+1,n+1)-SUM(E) -SUM(D)',  &
!	ABS( A(m+1,n+1) - SUM(D(1:i-1+nb)) - SUM(E(1:i-2+nb)) - SUM( A(m+1, 1:n) ) )
END SUBROUTINE

FUNCTION SUMM( M, N, A, LDA )
INTEGER :: I, J, M, N, LDA
DOUBLE PRECISION :: SUMM, A(LDA, *)
SUMM=0
DO J=1,M
	DO I=1,N
		SUMM = SUMM + A(I, J)
	END DO
END DO
END FUNCTION SUMM

SUBROUTINE PMAT( a, lda, m, n )
DOUBLE PRECISION :: a( lda, * )
INTEGER :: m, n, lda


END SUBROUTINE PMAT
