subroutine ft2_dgemv(trans,m,n,alpha,a,lda,x,incx,beta,y,incy)
  implicit none
  ! .. scalar arguments ..
  double precision alpha,beta
  integer incx,incy,lda,m,n
  character trans
  ! *     ..
  ! *     .. array arguments ..
  double precision a(lda,*),x(incx,*),y(incy,*)

  logical not, lsame
  double precision w(max(m,n))
  integer i, j
  
  not = lsame( trans, 'N' )

  do i=1,max(m,n)
     w(i) = i
  end do
  
  if( not ) then
     do j=1,n
        A(m+1, j) = SUM( A(1:m, j) )
        A(m+2, j) = DOT_PRODUCT( A(1:m, j), W(1:m) )
     end do
     y(1,m+1) = SUM( y(1, 1:m) )
     y(1,m+2) = DOT_PRODUCT( y(1,1:m), w(1:m) )
     call dgemv(trans,m+2,n,alpha,a,lda,x,incx,beta,y,incy)
     call chkvec( m, y, incy, w )
  else
     do i=1,m
        A(i, n+1) = SUM( A(i, 1:n) )
        A(i, n+2) = DOT_PRODUCT( A(i, 1:n), W(1:n) )
     end do
     y(1,n+1) = SUM( y(1, 1:n) )
     y(1,n+2) = DOT_PRODUCT( y(1,1:n), w(1:n) )
     call dgemv(trans,m,n+2,alpha,a,lda,x,incx,beta,y,incy)
     call chkvec( n, y, incy, w )
  end if
  
end subroutine ft2_dgemv
