subroutine ft2_dgemm(TRANSA,TRANSB,M,N,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
  implicit none
  
  ! .. Scalar Arguments ..        
  DOUBLE PRECISION ALPHA,BETA
  INTEGER K,LDA,LDB,LDC,M,N
  CHARACTER TRANSA,TRANSB
  ! ..
  ! .. Array Arguments ..
  DOUBLE PRECISION A(LDA,*),B(LDB,*),C(LDC,*)
  ! ..
  LOGICAL NOTA,NOTB,RMIS,CMIS, LSAME
  DOUBLE PRECISION W(MAX(M,N)), D1, D2, EPS
  INTEGER I, J
  PARAMETER (EPS=1.0D-3)

  
  nota = lsame(transa, 'N')
  notb = lsame(transb, 'N')
  ! generate checksums
  do i=1,max(m,n)
     w(i) = i
  end do
  
  if( nota ) then
     do j=1,k
        A(m+1, j) = SUM( A(1:m, j) )
     end do
  else
     do j=1,k
        A(j, m+1) = SUM( A(j, 1:m) )
     end do
  end if

  if( notb ) then
     do i=1,k
        B(i, n+1) = SUM( B(i, 1:n) )
     end do
  else
     do i=1,k
        B(n+1,i) = SUM( B(1:n, i ) )
     end do
  end if

  do i=1,m
     C(i, n+1) = SUM( C(i, 1:n) )
  end do
  do i=1,n
     C(m+1, i) = SUM( C(1:m, i ))
  end do
  
  call dgemm(TRANSA,TRANSB,M+1,N+1,K,ALPHA,A,LDA,B,LDB,BETA,C,LDC)
  ! C(2,2) = 8                    
  ! detection and correction.
  RMIS = .false.
  CMIS = .false.
  do i=1,m
     D1 = C(i, n+1) - SUM( C(i, 1:n) )
     if( abs(D1) > EPS ) then
        RMIS = .true.
        exit
     end if
  end do
  do j=1,n
     D2 = C(m+1, j) - SUM( C(1:m, j ))
     if( abs(D2) > EPS ) then
        CMIS = .true.
        exit
     end if
  end do

  if( RMIS .OR. CMIS ) then
     print *, '[warn] ft2_dgemm: error mismatch detected.'
  end if

  if( RMIS .AND. CMIS ) then
     print *, '[info] ft2_dgemm: try to correct. i,j=', i, j
     C(i,j) = C(i,j) + D1
  end if
  
end subroutine ft2_dgemm
