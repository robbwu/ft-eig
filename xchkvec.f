      program xchkvec
      implicit none
      double precision A(10,10), W(10)
      integer i, j

      do i=1, 10
         do j=1, 10
            A(i,j) = i+j
         end do
      end do

      do i=1,8
         w(i) = i
      end do

      A(9,1) = 0
      A(10,1) = 0
      A(1,9) = 0
      A(1,10) = 0
      do i=1,8
         A(9,1) = A(9,1) + A(i,1)
         A(1,9) = A(1,9) + A(1,i)
         A(10,1) = A(10,1) + A(i,1) * W(i)
         A(1,10) = A(1,10) + A(1,i) * W(i)
      end do

      A(9,1) = A(9,1) - 3
      A(1,9) = A(1,9) - 3
      A(10,1) = A(10,1) - 3*3
      A(1,10) = A(1,10) - 3*3

      call chkvec( 8, A(1,1), 1, W )
      call chkvec( 8, A(1,1), 10, W )
      write(*,"(100g12.5)") ( A(i,1), i=1,10 )
      write(*,"(100g12.5)") ( A(1,i), i=1,10 )
      end program
