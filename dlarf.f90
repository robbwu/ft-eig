!> \brief \b DLARF applies an elementary reflector to a general rectangular matrix.

! Code converted using TO_F90 by Alan Miller
! Date: 2016-02-19  Time: 16:04:48

!  =========== DOCUMENTATION ===========

! Online html documentation available at
!            http://www.netlib.org/lapack/explore-html/

!> \htmlonly
!> Download DLARF + dependencies
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.tgz?format=tgz&filename=/lapack/lapack_routine/dlarf.f">
!> [TGZ]</a>
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.zip?format=zip&filename=/lapack/lapack_routine/dlarf.f">
!> [ZIP]</a>
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.txt?format=txt&filename=/lapack/lapack_routine/dlarf.f">
!> [TXT]</a>
!> \endhtmlonly

!  Definition:
!  ===========

!       SUBROUTINE DLARF( SIDE, M, N, V, INCV, TAU, C, LDC, WORK )

!       .. Scalar Arguments ..
!       CHARACTER          SIDE
!       INTEGER            INCV, LDC, M, N
!       DOUBLE PRECISION   TAU
!       ..
!       .. Array Arguments ..
!       DOUBLE PRECISION   C( LDC, * ), V( * ), WORK( * )
!       ..


!> \par Purpose:
!  =============
!>
!> \verbatim
!>
!> DLARF applies a real elementary reflector H to a real m by n matrix
!> C, from either the left or the right. H is represented in the form
!>
!>       H = I - tau * v * v**T
!>
!> where tau is a real scalar and v is a real vector.
!>
!> If tau = 0, then H is taken to be the unit matrix.
!> \endverbatim

!  Arguments:
!  ==========

!> \param[in] SIDE
!> \verbatim
!>          SIDE is CHARACTER*1
!>          = 'L': form  H * C
!>          = 'R': form  C * H
!> \endverbatim
!>
!> \param[in] M
!> \verbatim
!>          M is INTEGER
!>          The number of rows of the matrix C.
!> \endverbatim
!>
!> \param[in] N
!> \verbatim
!>          N is INTEGER
!>          The number of columns of the matrix C.
!> \endverbatim
!>
!> \param[in] V
!> \verbatim
!>          V is DOUBLE PRECISION array, dimension
!>                     (1 + (M-1)*abs(INCV)) if SIDE = 'L'
!>                  or (1 + (N-1)*abs(INCV)) if SIDE = 'R'
!>          The vector v in the representation of H. V is not used if
!>          TAU = 0.
!> \endverbatim
!>
!> \param[in] INCV
!> \verbatim
!>          INCV is INTEGER
!>          The increment between elements of v. INCV <> 0.
!> \endverbatim
!>
!> \param[in] TAU
!> \verbatim
!>          TAU is DOUBLE PRECISION
!>          The value tau in the representation of H.
!> \endverbatim
!>
!> \param[in,out] C
!> \verbatim
!>          C is DOUBLE PRECISION array, dimension (LDC,N)
!>          On entry, the m by n matrix C.
!>          On exit, C is overwritten by the matrix H * C if SIDE = 'L',
!>          or C * H if SIDE = 'R'.
!> \endverbatim
!>
!> \param[in] LDC
!> \verbatim
!>          LDC is INTEGER
!>          The leading dimension of the array C. LDC >= max(1,M).
!> \endverbatim
!>
!> \param[out] WORK
!> \verbatim
!>          WORK is DOUBLE PRECISION array, dimension
!>                         (N) if SIDE = 'L'
!>                      or (M) if SIDE = 'R'
!> \endverbatim

!  Authors:
!  ========

!> \author Univ. of Tennessee
!> \author Univ. of California Berkeley
!> \author Univ. of Colorado Denver
!> \author NAG Ltd.

!> \date September 2012

!> \ingroup doubleOTHERauxiliary

!  =====================================================================

SUBROUTINE ft_dlarf( side, m, n, v, incv, tau, c, ldc, work )

!  -- LAPACK auxiliary routine (version 3.4.2) --
!  -- LAPACK is a software package provided by Univ. of Tennessee,    --
!  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
!     September 2012

!     .. Scalar Arguments ..

CHARACTER (LEN=1), INTENT(IN)            :: side
INTEGER, INTENT(IN)                      :: m
INTEGER, INTENT(IN)                      :: n
DOUBLE PRECISION, INTENT(IN OUT)         :: v( * )
INTEGER, INTENT(IN)                      :: incv
DOUBLE PRECISION, INTENT(IN)             :: tau
DOUBLE PRECISION, INTENT(IN)             :: c( ldc, * )
INTEGER, INTENT(IN OUT)                  :: ldc
DOUBLE PRECISION, INTENT(IN OUT)         :: work( * )



!     ..
!     .. Array Arguments ..

!     ..

!  =====================================================================

!     .. Parameters ..

DOUBLE PRECISION, PARAMETER :: one = 1.0D+0
DOUBLE PRECISION, PARAMETER :: zero = 0.0D+0
!     ..
!     .. Local Scalars ..
LOGICAL :: applyleft
INTEGER :: i, lastv, lastc
!     ..
!     .. External Subroutines ..
EXTERNAL           dgemv, dger
!     ..
!     .. External Functions ..
LOGICAL :: lsame
INTEGER :: iladlr, iladlc
EXTERNAL           lsame, iladlr, iladlc
!     ..
!     .. Executable Statements ..

applyleft = lsame( side, 'L' )
lastv = 0
lastc = 0
IF( tau /= zero ) THEN
!     Set up variables for scanning V.  LASTV begins pointing to the end
!     of V.
  IF( applyleft ) THEN
    lastv = m
  ELSE
    lastv = n
  END IF
  IF( incv > 0 ) THEN
    i = 1 + (lastv-1) * incv
  ELSE
    i = 1
  END IF
!     Look for the last non-zero row in V.
  DO WHILE( lastv > 0 .AND. v( i ) == zero )
    lastv = lastv - 1
    i = i - incv
  END DO
  IF( applyleft ) THEN
!     Scan for the last non-zero column in C(1:lastv,:).
    lastc = iladlc(lastv, n, c, ldc)
  ELSE
!     Scan for the last non-zero row in C(:,1:lastv).
    lastc = iladlr(m, lastv, c, ldc)
  END IF
END IF





IF( applyleft ) THEN
	IF( lastv .NE. m .OR. lastc .NE. n ) THEN
		PRINT *, 'lastv,lastc,m,n', lastv, lastc, m, n
		PRINT *,'Reassigning lastv=m lastc=n'
		lastv=m; lastc=n;
  END IF
ELSE
	IF( lastv .NE. n .OR. lastc .NE. m ) THEN
		PRINT *, 'lastv,lastc,n,m', lastv, lastc,n,m
		PRINT *,'Reassigning lastv=n, lastc=m'
		lastv=n; lastc=m;
  END IF	
END IF

!     Note that lastc.eq.0 renders the BLAS operations null; no special
!     case is needed at this level.
IF( applyleft ) THEN
  
!        Form  H * C
  
  IF( lastv > 0 ) THEN
    
!           w(1:lastc,1) := C(1:lastv,1:lastc)**T * v(1:lastv,1)
    
!    CALL dgemv( 'Transpose', lastv, lastc, one, c, ldc, v, incv,  &
!        zero, work, 1 )
    CALL dgemv( 'Transpose', lastv, lastc+1, one, c, ldc, v, incv,  &
        zero, work, 1 )
    
!           C(1:lastv,1:lastc) := C(...) - v(1:lastv,1) * w(1:lastc,1)**T
    
    CALL dger( lastv+1, lastc+1, -tau, v, incv, work, 1, c, ldc )
  END IF
ELSE
  
!        Form  C * H
  
  IF( lastv > 0 ) THEN
    
!           w(1:lastc,1) := C(1:lastc,1:lastv) * v(1:lastv,1)
    
!    CALL dgemv( 'No transpose', lastc, lastv, one, c, ldc,  &
!        v, incv, zero, work, 1 )
    CALL dgemv( 'No transpose', lastc+1, lastv, one, c, ldc,  &
        v, incv, zero, work, 1 )
    
!           C(1:lastc,1:lastv) := C(...) - w(1:lastc,1) * v(1:lastv,1)**T
    
    CALL dger( lastc+1, lastv+1, -tau, work, 1, v, incv, c, ldc )
  END IF
END IF
RETURN

!     End of DLARF

END SUBROUTINE ft_dlarf
