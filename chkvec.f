C     ************************************************************
C     CHKVEC assumes a vector of two correct checksums and at most one
C     incorrect element. CHKVEC tries to locate and correct the
C     erroneous element.
C
C     Arguments:
C     N: length of vector
C     X: the vector starting element
C     INCX: 1 for column vector; LDX for row vector
C     W: the second checksum weights; the first weights are all one
C     ************************************************************

      subroutine chkvec( n, x, incx, w )
      implicit none
C
C     parameters
C
      integer n, incx
      double precision x(incx, *), w(*)
C
C     local variables
C
      integer i
      double precision d1, d2, eps, r
      logical wmatch, mismatch
      parameter ( eps = 1.0d-3 )

      if( n.le.0 ) return
      wmatch = .false.
      mismatch = .false.
      d1 = 0.0D0
      do i=1,n
         d1 = d1 + x( 1, i )
      end do
      d1 = d1 - x( 1, n+1 )
      if( abs(d1) < eps ) then

         return
      else
         mismatch = .true.
         print *, '[warn] chkvec: error first chksum mismatch!'
     $        , __FILE__, __LINE__
      end if


      d2 = 0.0D0
      do i=1,n
         d2 = d2 + w( i ) * x( 1, i )
      end do
      d2 = d2 - x( 1, n+2 )
      if( abs(d2) < eps ) then

c$$$         return
      else
         print *, '[warn] chkvec: error second chksum mismatch!',
     $        d2,n, __FILE__, __LINE__
         mismatch = .true.
      end if

      if( mismatch ) then
         r = d2 / d1

         do i=1,n
            if( abs( w(i) - r ) < eps ) then
               wmatch = .true.
               exit
            end if
         end do

         if( .not. wmatch ) then
            print *, '[warn] chkvec: cannot locate error. abort.',
     $           __FILE__, __LINE__
         else
            print *, '[info] chkvec: wmatch! i=', i,
     $           __FILE__, __LINE__
            x( 1,i ) = x( 1,i ) - d1
         end if
      end if

      end subroutine
