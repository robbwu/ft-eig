!> \brief \b DLABRD reduces the first nb rows and columns of a general matrix to a bidiagonal form.

! Code converted using TO_F90 by Alan Miller
! Date: 2016-02-16  Time: 17:32:39

!  =========== DOCUMENTATION ===========

! Online html documentation available at
!            http://www.netlib.org/lapack/explore-html/

!> \htmlonly
!> Download DLABRD + dependencies
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.tgz?format=tgz&filename=/lapack/lapack_routine/dlabrd.f">
!> [TGZ]</a>
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.zip?format=zip&filename=/lapack/lapack_routine/dlabrd.f">
!> [ZIP]</a>
!> <a href="http://www.netlib.org/cgi-bin/netlibfiles.txt?format=txt&filename=/lapack/lapack_routine/dlabrd.f">
!> [TXT]</a>
!> \endhtmlonly

!  Definition:
!  ===========

!       SUBROUTINE FT_DLABRD( M, N, NB, A, LDA, D, E, TAUQ, TAUP, X, LDX, Y,
!                          LDY )

!       .. Scalar Arguments ..
!       INTEGER            LDA, LDX, LDY, M, N, NB
!       ..
!       .. Array Arguments ..
!       DOUBLE PRECISION   A( LDA, * ), D( * ), E( * ), TAUP( * ),
!      $                   TAUQ( * ), X( LDX, * ), Y( LDY, * )
!       ..


!> \par Purpose:
!  =============
!>
!> \verbatim
!>
!> DLABRD reduces the first NB rows and columns of a real general
!> m by n matrix A to upper or lower bidiagonal form by an orthogonal
!> transformation Q**T * A * P, and returns the matrices X and Y which
!> are needed to apply the transformation to the unreduced part of A.
!>
!> If m >= n, A is reduced to upper bidiagonal form; if m < n, to lower
!> bidiagonal form.
!>
!> This is an auxiliary routine called by DGEBRD
!> \endverbatim

!  Arguments:
!  ==========

!> \param[in] M
!> \verbatim
!>          M is INTEGER
!>          The number of rows in the matrix A.
!> \endverbatim
!>
!> \param[in] N
!> \verbatim
!>          N is INTEGER
!>          The number of columns in the matrix A.
!> \endverbatim
!>
!> \param[in] NB
!> \verbatim
!>          NB is INTEGER
!>          The number of leading rows and columns of A to be reduced.
!> \endverbatim
!>
!> \param[in,out] A
!> \verbatim
!>          A is DOUBLE PRECISION array, dimension (LDA,N)
!>          On entry, the m by n general matrix to be reduced.
!>          On exit, the first NB rows and columns of the matrix are
!>          overwritten; the rest of the array is unchanged.
!>          If m >= n, elements on and below the diagonal in the first NB
!>            columns, with the array TAUQ, represent the orthogonal
!>            matrix Q as a product of elementary reflectors; and
!>            elements above the diagonal in the first NB rows, with the
!>            array TAUP, represent the orthogonal matrix P as a product
!>            of elementary reflectors.
!>          If m < n, elements below the diagonal in the first NB
!>            columns, with the array TAUQ, represent the orthogonal
!>            matrix Q as a product of elementary reflectors, and
!>            elements on and above the diagonal in the first NB rows,
!>            with the array TAUP, represent the orthogonal matrix P as
!>            a product of elementary reflectors.
!>          See Further Details.
!> \endverbatim
!>
!> \param[in] LDA
!> \verbatim
!>          LDA is INTEGER
!>          The leading dimension of the array A.  LDA >= max(1,M).
!> \endverbatim
!>
!> \param[out] D
!> \verbatim
!>          D is DOUBLE PRECISION array, dimension (NB)
!>          The diagonal elements of the first NB rows and columns of
!>          the reduced matrix.  D(i) = A(i,i).
!> \endverbatim
!>
!> \param[out] E
!> \verbatim
!>          E is DOUBLE PRECISION array, dimension (NB)
!>          The off-diagonal elements of the first NB rows and columns of
!>          the reduced matrix.
!> \endverbatim
!>
!> \param[out] TAUQ
!> \verbatim
!>          TAUQ is DOUBLE PRECISION array dimension (NB)
!>          The scalar factors of the elementary reflectors which
!>          represent the orthogonal matrix Q. See Further Details.
!> \endverbatim
!>
!> \param[out] TAUP
!> \verbatim
!>          TAUP is DOUBLE PRECISION array, dimension (NB)
!>          The scalar factors of the elementary reflectors which
!>          represent the orthogonal matrix P. See Further Details.
!> \endverbatim
!>
!> \param[out] X
!> \verbatim
!>          X is DOUBLE PRECISION array, dimension (LDX,NB)
!>          The m-by-nb matrix X required to update the unreduced part
!>          of A.
!> \endverbatim
!>
!> \param[in] LDX
!> \verbatim
!>          LDX is INTEGER
!>          The leading dimension of the array X. LDX >= max(1,M).
!> \endverbatim
!>
!> \param[out] Y
!> \verbatim
!>          Y is DOUBLE PRECISION array, dimension (LDY,NB)
!>          The n-by-nb matrix Y required to update the unreduced part
!>          of A.
!> \endverbatim
!>
!> \param[in] LDY
!> \verbatim
!>          LDY is INTEGER
!>          The leading dimension of the array Y. LDY >= max(1,N).
!> \endverbatim

!  Authors:
!  ========

!> \author Univ. of Tennessee
!> \author Univ. of California Berkeley
!> \author Univ. of Colorado Denver
!> \author NAG Ltd.

!> \date September 2012

!> \ingroup doubleOTHERauxiliary

!> \par Further Details:
!  =====================
!>
!> \verbatim
!>
!>  The matrices Q and P are represented as products of elementary
!>  reflectors:
!>
!>     Q = H(1) H(2) . . . H(nb)  and  P = G(1) G(2) . . . G(nb)
!>
!>  Each H(i) and G(i) has the form:
!>
!>     H(i) = I - tauq * v * v**T  and G(i) = I - taup * u * u**T
!>
!>  where tauq and taup are real scalars, and v and u are real vectors.
!>
!>  If m >= n, v(1:i-1) = 0, v(i) = 1, and v(i:m) is stored on exit in
!>  A(i:m,i); u(1:i) = 0, u(i+1) = 1, and u(i+1:n) is stored on exit in
!>  A(i,i+1:n); tauq is stored in TAUQ(i) and taup in TAUP(i).
!>
!>  If m < n, v(1:i) = 0, v(i+1) = 1, and v(i+1:m) is stored on exit in
!>  A(i+2:m,i); u(1:i-1) = 0, u(i) = 1, and u(i:n) is stored on exit in
!>  A(i,i+1:n); tauq is stored in TAUQ(i) and taup in TAUP(i).
!>
!>  The elements of the vectors v and u together form the m-by-nb matrix
!>  V and the nb-by-n matrix U**T which are needed, with X and Y, to apply
!>  the transformation to the unreduced part of the matrix, using a block
!>  update of the form:  A := A - V*Y**T - X*U**T.
!>
!>  The contents of A on exit are illustrated by the following examples
!>  with nb = 2:
!>
!>  m = 6 and n = 5 (m > n):          m = 5 and n = 6 (m < n):
!>
!>    (  1   1   u1  u1  u1 )           (  1   u1  u1  u1  u1  u1 )
!>    (  v1  1   1   u2  u2 )           (  1   1   u2  u2  u2  u2 )
!>    (  v1  v2  a   a   a  )           (  v1  1   a   a   a   a  )
!>    (  v1  v2  a   a   a  )           (  v1  v2  a   a   a   a  )
!>    (  v1  v2  a   a   a  )           (  v1  v2  a   a   a   a  )
!>    (  v1  v2  a   a   a  )
!>
!>  where a denotes an element of the original matrix which is unchanged,
!>  vi denotes an element of the vector defining H(i), and ui an element
!>  of the vector defining G(i).
!> \endverbatim
!>
!  =====================================================================

! Additional notes for the Fault Tolerance (FT):
! The matrix A must accomondate *nc* more rows and columns than specified
! by m, n. X, and Y should accomondate *nc* more row.
!
! info = 0: no error detected
! info = -1: error detected
!  =====================================================================


SUBROUTINE ft_dlabrd( m, n, nb, a, lda, d, e, tauq, taup, x, ldx, y, ldy, &
     &w, info)

!  -- LAPACK auxiliary routine (version 3.4.2) --
!  -- LAPACK is a software package provided by Univ. of Tennessee,    --
!  -- Univ. of California Berkeley, Univ. of Colorado Denver and NAG Ltd..--
!     September 2012

!     .. Scalar Arguments ..

INTEGER, INTENT(IN)                      :: m
INTEGER, INTENT(IN)                      :: n
INTEGER, INTENT(IN)                      :: nb
DOUBLE PRECISION, INTENT(IN OUT)         :: a( lda, * )
INTEGER, INTENT(IN OUT)                  :: lda
DOUBLE PRECISION, INTENT(OUT)            :: d( * )
DOUBLE PRECISION, INTENT(OUT)            :: e( * )
DOUBLE PRECISION, INTENT(IN OUT)         :: tauq( * )
DOUBLE PRECISION, INTENT(IN OUT)         :: taup( * )
DOUBLE PRECISION, INTENT(IN OUT)         :: x( ldx, * )
INTEGER, INTENT(IN OUT)                  :: ldx
DOUBLE PRECISION, INTENT(IN OUT)         :: y( ldy, * )
INTEGER, INTENT(IN OUT)                  :: ldy
DOUBLE PRECISION, INTENT(IN) :: w( * )
INTEGER, INTENT(OUT) :: info
!     ..
!     .. Array Arguments ..

!     ..

!  =====================================================================

!     .. Parameters ..

DOUBLE PRECISION, PARAMETER :: zero = 0.0D0
DOUBLE PRECISION, PARAMETER :: one = 1.0D0
!     ..
!     .. Local Scalars ..
INTEGER :: i, j
!     ..
!     .. External Subroutines ..
EXTERNAL           dgemv, dlarfg, dscal
!     ..
!     .. Intrinsic Functions ..
INTRINSIC          MIN
!     ..
!     .. Executable Statements ..
DOUBLE PRECISION :: diff, eps
PARAMETER ( EPS = 1.0D-3 )

info = 0
!     Quick return if possible

IF( m <= 0 .OR. n <= 0 ) RETURN

IF( m >= n ) THEN

!        Reduce to upper bidiagonal form

  DO  i = 1, nb

!           Update A(i:m,i)

    CALL dgemv( 'No transpose', m-i+1+2, i-1, -one, a( i, 1 ),  &
        lda, y( i, 1 ), ldy, one, a( i, i ), 1 )
    CALL dgemv( 'No transpose', m-i+1+2, i-1, -one, x( i, 1 ),  &
        ldx, a( 1, i ), 1, one, a( i, i ), 1 )
!           Generate reflection Q(i) to annihilate A(i+1:m,i)
    ! verify checksums
    ! CALL chkvec( m-i+1, a( i, i ), 1, w( i ) )
    if( i.gt.1 ) then
       diff = SUM( a( i:m , i ) ) + E( i - 1 ) - a( m+1, i )
       if( abs(diff) > EPS ) then
          print *, '[warn]ft_dlabrd: error detected'
          info = -1
          return
       end if
    end if
    CALL dlarfg( m-i+1, a( i, i ), a( MIN( i+1, m ), i ), 1, tauq( i ) )
    ! a( m+1, i ) = a( m+1, i ) - a( i, i )
    ! IF( i > 1 )  a( m+1, i ) = a( m+1, i ) - e( i-1 )
    ! a( m+1, i ) = -a( m+1, i ) / ( tauq( i ) * a( i, i ) )
!	recalculate checksum of U
    a( m+1, i ) = SUM( a( i+1:m, i ) ) + 1.0
    a( m+2, i ) = DOT_PRODUCT( a( i+1:m, i ), w( i+1:m ) ) + 1.0*w( i )
    d( i ) = a( i, i )
    IF( i < n ) THEN
      a( i, i ) = one

!              Compute Y(i+1:n,i)

      CALL dgemv( 'Transpose', m-i+1, n-i+2, one, a( i, i+1 ),  &
          lda, a( i, i ), 1, zero, y( i+1, i ), 1 )
      CALL dgemv( 'Transpose', m-i+1, i-1, one, a( i, 1 ), lda,  &
          a( i, i ), 1, zero, y( 1, i ), 1 )
      CALL dgemv( 'No transpose', n-i+2, i-1, -one, y( i+1, 1 ),  &
          ldy, y( 1, i ), 1, one, y( i+1, i ), 1 )
      CALL dgemv( 'Transpose', m-i+1, i-1, one, x( i, 1 ), ldx,  &
          a( i, i ), 1, zero, y( 1, i ), 1 )
      CALL dgemv( 'Transpose', i-1, n-i+2, -one, a( 1, i+1 ),  &
          lda, y( 1, i ), 1, one, y( i+1, i ), 1 )
      CALL dscal( n-i+2, tauq( i ), y( i+1, i ), 1 )

!              Update A(i,i+1:n)

      CALL dgemv( 'No transpose', n-i+2, i, -one, y( i+1, 1 ),  &
          ldy, a( i, 1 ), lda, one, a( i, i+1 ), lda )
      CALL dgemv( 'Transpose', i-1, n-i+2, -one, a( 1, i+1 ),  &
          lda, x( i, 1 ), ldx, one, a( i, i+1 ), lda )
      ! CALL chkvec( n-i, a( i, i+1 ), lda, w( i+1 ) )
      diff = sum( a(i, i+1:n) ) + D(i) - a(i, n+1)
      if( abs(diff) > EPS ) then
         print *, '[warn]ft_dlabrd: error detected'
         info = -1
         return
      end if
!              Generate reflection P(i) to annihilate A(i,i+2:n)

      CALL dlarfg( n-i, a( i, i+1 ), a( i, MIN( i+2, n ) ), lda, taup( i ) )
      ! a( i, n+1 ) = a( i, n+1 ) - a( i, i+1 ) - d( i )
      ! a( i, n+1 ) = - a( i, n+1 ) / ( taup( i ) * a( i, i+1 ) )
      a( i, n+1 ) = SUM( a(i, i+2:n ) ) + 1.0
      a( i, n+2 ) = DOT_PRODUCT( a(i, i+2:n ), w( i+2:n ) ) + 1.0 * w( i+ 1 )

      e( i ) = a( i, i+1 )
      a( i, i+1 ) = one

!              Compute X(i+1:m,i)

      CALL dgemv( 'No transpose', m-i+2, n-i, one, a( i+1, i+1 ),  &
          lda, a( i, i+1 ), lda, zero, x( i+1, i ), 1 )
      CALL dgemv( 'Transpose', n-i, i, one, y( i+1, 1 ), ldy,  &
          a( i, i+1 ), lda, zero, x( 1, i ), 1 )
      CALL dgemv( 'No transpose', m-i+2, i, -one, a( i+1, 1 ),  &
          lda, x( 1, i ), 1, one, x( i+1, i ), 1 )
      CALL dgemv( 'No transpose', i-1, n-i, one, a( 1, i+1 ),  &
          lda, a( i, i+1 ), lda, zero, x( 1, i ), 1 )
      CALL dgemv( 'No transpose', m-i+2, i-1, -one, x( i+1, 1 ),  &
          ldx, x( 1, i ), 1, one, x( i+1, i ), 1 )
      CALL dscal( m-i+2, taup( i ), x( i+1, i ), 1 )
    END IF
  END DO
ELSE
   PRINT *, 'Fatal error: DLABRD: lower bidiagonal form not implemented.'
   RETURN
!        Reduce to lower bidiagonal form

  DO  i = 1, nb

!           Update A(i,i:n)

    CALL dgemv( 'No transpose', n-i+1, i-1, -one, y( i, 1 ),  &
        ldy, a( i, 1 ), lda, one, a( i, i ), lda )
    CALL dgemv( 'Transpose', i-1, n-i+1, -one, a( 1, i ), lda,  &
        x( i, 1 ), ldx, one, a( i, i ), lda )

!           Generate reflection P(i) to annihilate A(i,i+1:n)

    CALL dlarfg( n-i+1, a( i, i ), a( i, MIN( i+1, n ) ), lda, taup( i ) )
    d( i ) = a( i, i )
    IF( i < m ) THEN
      a( i, i ) = one

!              Compute X(i+1:m,i)

      CALL dgemv( 'No transpose', m-i, n-i+1, one, a( i+1, i ),  &
          lda, a( i, i ), lda, zero, x( i+1, i ), 1 )
      CALL dgemv( 'Transpose', n-i+1, i-1, one, y( i, 1 ), ldy,  &
          a( i, i ), lda, zero, x( 1, i ), 1 )
      CALL dgemv( 'No transpose', m-i, i-1, -one, a( i+1, 1 ),  &
          lda, x( 1, i ), 1, one, x( i+1, i ), 1 )
      CALL dgemv( 'No transpose', i-1, n-i+1, one, a( 1, i ),  &
          lda, a( i, i ), lda, zero, x( 1, i ), 1 )
      CALL dgemv( 'No transpose', m-i, i-1, -one, x( i+1, 1 ),  &
          ldx, x( 1, i ), 1, one, x( i+1, i ), 1 )
      CALL dscal( m-i, taup( i ), x( i+1, i ), 1 )

!              Update A(i+1:m,i)

      CALL dgemv( 'No transpose', m-i, i-1, -one, a( i+1, 1 ),  &
          lda, y( i, 1 ), ldy, one, a( i+1, i ), 1 )
      CALL dgemv( 'No transpose', m-i, i, -one, x( i+1, 1 ),  &
          ldx, a( 1, i ), 1, one, a( i+1, i ), 1 )

!              Generate reflection Q(i) to annihilate A(i+2:m,i)

      CALL dlarfg( m-i, a( i+1, i ), a( MIN( i+2, m ), i ), 1, tauq( i ) )
      e( i ) = a( i+1, i )
      a( i+1, i ) = one

!              Compute Y(i+1:n,i)

      CALL dgemv( 'Transpose', m-i, n-i, one, a( i+1, i+1 ),  &
          lda, a( i+1, i ), 1, zero, y( i+1, i ), 1 )
      CALL dgemv( 'Transpose', m-i, i-1, one, a( i+1, 1 ), lda,  &
          a( i+1, i ), 1, zero, y( 1, i ), 1 )
      CALL dgemv( 'No transpose', n-i, i-1, -one, y( i+1, 1 ),  &
          ldy, y( 1, i ), 1, one, y( i+1, i ), 1 )
      CALL dgemv( 'Transpose', m-i, i, one, x( i+1, 1 ), ldx,  &
          a( i+1, i ), 1, zero, y( 1, i ), 1 )
      CALL dgemv( 'Transpose', i, n-i, -one, a( 1, i+1 ), lda,  &
          y( 1, i ), 1, one, y( i+1, i ), 1 )
      CALL dscal( n-i, tauq( i ), y( i+1, i ), 1 )
    END IF
  END DO
END IF
RETURN

!     End of DLABRD

END SUBROUTINE ft_dlabrd
